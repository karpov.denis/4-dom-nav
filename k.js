(function() {
  // constants
  const markerStyle = `
    <style>
      .DOM-elem-marked {
        border: 5px red solid !important;
      }
    </style>
  `;
  const widgetStyles = `
    .searcherWrap {
      display: flex;
      flex-direction: column;
      justify-content: space-around;
      z-index:10000;
      text-align: center;
      border: 1px solid #ff0;
      width: 300px;
      background-color: grey;
      color: #fff;
      font-size: 18px;
      box-sizing: box-border;
      padding: 10px 10px;
    }

    .searcher-container {
      display: flex;
      justify-content: space-between;
      align-items: center;
    }

    .searcher__header {
      text-align: center;
      margin-left: 40px;
      user-select: none;
    }
    
    .searcher__input {
      color: black;
      background-color: white;
      width: 74%; 
      box-sizing: border-box;
      padding: 5px;
      margin: 10px 5px 10px 0;
    }
    
    .searcher__btn {
      color: black;
      background-color: white;
      width: 26%;
      height: 15%;
      box-sizing: border-box;
      padding: 3px;
      border-radius: 25px;
      cursor: pointer;
    }

    .searcher__btn--close {
      box-sizing: border-box;
      width: 17%;          
      text-align: center;
      font-weight: 600;
      padding: 0;
      border: 1px solid #F64C2B;
      background: #F02F17;
    }
    
    .searcher__btn:disabled {
      opacity: 0.3;
      background-color: grey; 
      cursor: auto; 
    }
  `;
  const widgetWrapStyles = {
    position: 'fixed',
    top: '100px',
    left: '100px',            
  };
  const searcherHeaderText = 'Search DOM element';
  let currentElem = null;

  // maps
  const elementsMap = [
    {
      name: 'searcherDOMTool',
      node: 'div',
      classNames: ['searcherWidget'],
      options: {
        style: widgetWrapStyles,
        draggable: 'true'
      },
    },
    {
      name: 'searcherWrap',
      node: 'div',
      classNames: ['searcherWrap'],
      options: {},
    },
    {
      name: 'searcherStyles',
      node: 'style',
      classNames: [],
      options: {
        innerHTML: widgetStyles
      },
    },
    {
      name: 'searcherHeaderBox',
      node: 'div',
      classNames: ['searcher-container'],
      options: {},
    },
    {
      name: 'searcherHeader',
      node: 'span',
      classNames: ['searcher__header'],
      options: {
        innerHTML: searcherHeaderText
      },
    },
    {
      name: 'closeBtn',
      node: 'input',
      classNames: ['searcher__btn', 'searcher__btn--close'],
      options: {
        type: 'submit',
        value: 'X'
      },
    },
    {
      name: 'searcherInputBox',
      node: 'div',
      classNames: ['searcher-container'],
      options: {},
    },
    {
      name: 'searcherInput',
      node: 'input',
      classNames: ['searcher__input'],
      options: {
        type: 'search',
        placeholder: 'print here'
      },
    },
    {
      name: 'searchBtn',
      node: 'input',
      classNames: ['searcher__btn'],
      options: {
        type: 'submit',
        disabled: 'disabled',
        value: 'Search'
      },
    },
    {
      name: 'prevBtn',
      node: 'input',
      classNames: ['searcher__btn'],
      options: {
        type: 'submit',
        disabled: 'disabled',
        value: 'Prev'
      },
    },
    {
      name: 'parentBtn',
      node: 'input',
      classNames: ['searcher__btn'],
      options: {
        type: 'submit',
        disabled: 'disabled',
        value: 'Parent'
      },
    },
    {
      name: 'childBtn',
      node: 'input',
      classNames: ['searcher__btn'],
      options: {
        type: 'submit',
        disabled: 'disabled',
        value: 'Child'
      },
    },
    {
      name: 'nextBtn',
      node: 'input',
      classNames: ['searcher__btn'],
      options: {
        type: 'submit',
        disabled: 'disabled',
        value: 'Next'
      },
    },
    {
      name: 'searcherBtnBox',
      node: 'div',
      classNames: ['searcher-container'],
      options: {},
    }
  ];
  const parentsChildrenMap = {
    searcherHeaderBox: ['searcherHeader', 'closeBtn'],
    searcherInputBox: ['searcherInput', 'searchBtn'],
    searcherBtnBox: ['prevBtn', 'parentBtn', 'childBtn', 'nextBtn'],
    searcherWrap: ['searcherHeaderBox', 'searcherInputBox', 'searcherBtnBox', 'searcherStyles'],
    shadowRoot: ['searcherWrap']
  };

  // functions
  const createElement = (node, classNames = [], options) => {
    const newElement = document.createElement(node);

    classNames.forEach(className => newElement.classList.add(className));

    Object.entries(options).forEach(([key, value]) => {
      if (key !== 'style') {
        newElement[key] = value;
      } else {
        Object.entries(value).forEach(([propertyName, value]) => newElement[key][propertyName] = value);
      } 
    });

    return newElement;
  };

  const createElements = elements => elements.reduce((acc, el) => {
    const { name, node, classNames, options } = el;
    acc[name] = createElement(node, classNames, options);

    return acc;
  }, {});

  const appendChildrenToParent = (parentNode, children) => children.forEach(childName => parentNode.appendChild(elements[childName]));

  const appendChildrenToParents = parentsChildrenMap =>
    Object.entries(parentsChildrenMap).forEach(([parentName, children]) => appendChildrenToParent(elements[parentName], children));

  const handleClick = event => {
    const { target } = event;
    const { type, value } = target;

    if(type !== 'submit') {
      return
    }

    removeMarkElem(currentElem);

    switch(value) {
      case 'X': closeTool();
        break;
      case 'Search':
        searchAction();
        break;
      case 'Prev':
        currentElem = currentElem.previousElementSibling;
        markElem(currentElem);
        break;
      case 'Next':
        currentElem = currentElem.nextElementSibling;
        markElem(currentElem);
        break;
      case 'Parent':
        currentElem = currentElem.parentElement;
        markElem(currentElem);
        break;
      case 'Child':
        currentElem = currentElem.firstElementChild;
        markElem(currentElem);
        break;
      default:
        break;
    };

    const { searchBtn, nextBtn, childBtn, parentBtn, prevBtn } = elements;
    const btnsObj = {
      'Search' : searchBtn,
      'Next' : nextBtn,
      'Child' : childBtn,
      'Parent' : parentBtn,
      'Prev' : prevBtn
    };

    activeDeactiveBtn(btnsObj, currentElem);
  };

  const closeTool = () => elements.searcherDOMTool.remove();

  const handleDragStart = event => {
    event.target.style.opacity = '0.6';
    let shiftX = event.clientX - event.target.getBoundingClientRect().left;
    let shiftY = event.clientY - event.target.getBoundingClientRect().top;
    event.dataTransfer.setData('text/plain', `${shiftX},${shiftY}`);
    event.dataTransfer.setData('target', `${event.target.className}`);
  };
 
  const handleDragOver = event => {
    event.preventDefault();
  };
 
  const handleDrop = event => {
    event.preventDefault();
    const shift = event.dataTransfer.getData('text/plain').split(',');
    const dropElem = document.querySelector(`.${event.dataTransfer.getData('target')}`);
    dropElem.style.left = `${event.clientX - Number(shift[0])}px`;
    dropElem.style.top = `${event.clientY - Number(shift[1])}px`;
    dropElem.style.opacity = '1';
  };

  const handleFocus = () => elements.searchBtn.disabled = 0;

  const addMarkStyleToPage = () => {
    document.head.insertAdjacentHTML('beforeend', markerStyle);
  };

  const markElem = elem => {
    if (elem !== null) {
      elem.classList.add('DOM-elem-marked');
    }
  };

  const removeMarkElem = elem => {
    if (elem !== null) {
      elem.classList.remove('DOM-elem-marked');
    }
  };

  const searchAction = () => {
    if (searcherInput.value === '') { 
      return;
    }

    currentElem = document.querySelector(`${searcherInput.value}`);
    elements.searcherInput.value = '';
    markElem(currentElem);
  };

  const activeDeactiveBtn = (obj, elem) => Object.values(obj).map((el) => el.disabled = !checkElemFamily(el, elem));

  const checkElemFamily = (btn, elem) => {
    let enableFlag = true;

    switch(btn.value) { 
      case 'Search':
        enableFlag = false;
        break;
      case 'Prev':
        enableFlag = Boolean(elem.previousElementSibling);
        break;
      case 'Next':
        enableFlag = Boolean(elem.nextElementSibling);
        break;
      case 'Parent':
        enableFlag = Boolean(elem.parentElement);
        break;
      case 'Child':
        enableFlag = Boolean(elem.firstElementChild);
        break;
      default:
        break;
    }

    return enableFlag;
  };

  // execution
  addMarkStyleToPage();
  const elements = createElements(elementsMap);
  const { searcherDOMTool, searcherInput, searcherWrap } = elements; 
  elements.shadowRoot = searcherDOMTool.attachShadow({ mode: 'open' });
  appendChildrenToParents(parentsChildrenMap);

  document.body.appendChild(searcherDOMTool);

  searcherDOMTool.addEventListener('dragstart', handleDragStart);
  document.addEventListener('dragover', handleDragOver); 
  document.addEventListener('drop', handleDrop);
  searcherInput.addEventListener('focus', handleFocus);
  searcherWrap.addEventListener('click', handleClick, false);
}());